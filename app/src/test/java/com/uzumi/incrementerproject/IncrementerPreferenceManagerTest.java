package com.uzumi.incrementerproject;

import android.content.SharedPreferences;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class IncrementerPreferenceManagerTest {

  private static final String NUMBER_KEY = "NUMBER_KEY";
  private static final String MAXIMUM_VALUE_KEY = "MAXIMUM_VALUE_KEY";
  private static final String STEP_KEY = "STEP_KEY";

  @Rule public MockitoRule rule = MockitoJUnit.rule();

  @Mock SharedPreferences sharedPreferences;
  @Mock SharedPreferences.Editor editor;

  private IncrementerPreferenceManager incrementerPreferenceManager;

  @Before
  public void setUp() {
    when(sharedPreferences.getInt(NUMBER_KEY, 0)).thenReturn(4);
    when(sharedPreferences.getInt(MAXIMUM_VALUE_KEY, 1000)).thenReturn(20);
    when(sharedPreferences.getInt(STEP_KEY, 1)).thenReturn(2);
    when(sharedPreferences.edit()).thenReturn(editor);
    when(editor.putInt(NUMBER_KEY, 4)).thenReturn(editor);
    when(editor.putInt(MAXIMUM_VALUE_KEY, 4)).thenReturn(editor);
    when(editor.putInt(STEP_KEY, 4)).thenReturn(editor);
    when(editor.commit()).thenReturn(true);
    incrementerPreferenceManager = new IncrementerPreferenceManager(sharedPreferences);
  }

  @Test
  public void getNumberTest() {
    assertEquals(4, incrementerPreferenceManager.getNumber());
  }

  @Test
  public void getMaximumTest() {
    assertEquals(20, incrementerPreferenceManager.getMaximumValue());
  }

  @Test
  public void getStepTest() {
    assertEquals(2, incrementerPreferenceManager.getStep());
  }

  @Test
  public void setNumberTest() {
    incrementerPreferenceManager.setNumber(4);
    verify(sharedPreferences).edit();
    verify(editor).putInt(NUMBER_KEY, 4);
    verify(editor).commit();
  }

  @Test
  public void setMaximumValueTest() {
    incrementerPreferenceManager.setMaximumValue(4);
    verify(sharedPreferences).edit();
    verify(editor).putInt(MAXIMUM_VALUE_KEY, 4);
    verify(editor).commit();
  }

  @Test
  public void setStepTest() {
    incrementerPreferenceManager.setStep(4);
    verify(sharedPreferences).edit();
    verify(editor).putInt(STEP_KEY, 4);
    verify(editor).commit();
  }
}
