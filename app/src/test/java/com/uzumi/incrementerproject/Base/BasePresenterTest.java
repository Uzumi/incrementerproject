package com.uzumi.incrementerproject.Base;

import com.uzumi.incrementerproject.IncrementerPreferenceManager;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static org.mockito.Mockito.when;

public class BasePresenterTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    IncrementerPreferenceManager incrementerPreferenceManager;

    BasePresenter<Object> basePresenter;

    @Before
    public void setUp() {
        when(incrementerPreferenceManager.getNumber()).thenReturn(10);
        when(incrementerPreferenceManager.getMaximumValue()).thenReturn(Integer.MAX_VALUE);
        when(incrementerPreferenceManager.getStep()).thenReturn(2);
        basePresenter = new BasePresenter(incrementerPreferenceManager);
    }

    @Test(expected = RuntimeException.class)
    public void doubleBindListenerTest() {
        basePresenter.bindListener(new Object());
        basePresenter.bindListener(new Object());
    }

    @Test
    public void bindListenerTest() {
        basePresenter.bindListener(new Object());
        assertNotNull(basePresenter.listener);
    }

    @Test(expected = RuntimeException.class)
    public void unbindNullListenerTest() {
        basePresenter.unbindListener();
    }

    @Test
    public void unbindListenerTest() {
        basePresenter.bindListener(new Object());
        basePresenter.unbindListener();
        assertNull(basePresenter.listener);
    }
}
