package com.uzumi.incrementerproject.Fragments.CounterFragment;

import com.uzumi.incrementerproject.IncrementerPreferenceManager;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CounterPresenterTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    IncrementerPreferenceManager incrementerPreferenceManager;
    @Mock
    CounterPresenter.Listener listener;

    CounterPresenter counterPresenter;

    @Before
    public void setUp() {
        when(incrementerPreferenceManager.getNumber()).thenReturn(8);
        when(incrementerPreferenceManager.getMaximumValue()).thenReturn(9);
        when(incrementerPreferenceManager.getStep()).thenReturn(2);
        counterPresenter = new CounterPresenter(incrementerPreferenceManager);
    }

    @Test
    public void bindListenerTest() {
        counterPresenter.bindListener(listener);
        verify(listener).setNumber(8);
    }

    @Test
    public void incrementNumberResetTest() {
        counterPresenter.bindListener(listener);
        counterPresenter.incrementNumber();
        verify(incrementerPreferenceManager).setNumber(0);
        verify(listener).setNumber(0);
    }

    @Test
    public void incrementNumberTest() {
        counterPresenter.bindListener(listener);
        counterPresenter.incrementNumber();
        counterPresenter.incrementNumber();
        verify(incrementerPreferenceManager).setNumber(2);
        verify(listener).setNumber(2);
    }

    @Test
    public void incrementNumberNullListenerTest() {
        counterPresenter.incrementNumber();
        verify(listener, never()).setNumber(0);
    }
}
