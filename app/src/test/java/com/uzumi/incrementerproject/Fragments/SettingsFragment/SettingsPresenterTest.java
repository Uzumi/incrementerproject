package com.uzumi.incrementerproject.Fragments.SettingsFragment;

import com.uzumi.incrementerproject.IncrementerPreferenceManager;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SettingsPresenterTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    IncrementerPreferenceManager incrementerPreferenceManager;
    @Mock
    SettingsPresenter.Listener listener;
    SettingsPresenter settingsPresenter;

    @Before
    public void setUp() {
        when(incrementerPreferenceManager.getNumber()).thenReturn(8);
        when(incrementerPreferenceManager.getMaximumValue()).thenReturn(9);
        when(incrementerPreferenceManager.getStep()).thenReturn(2);
        doNothing().when(listener).setStep(2);
        doNothing().when(listener).setMaximumValue(9);
        settingsPresenter = new SettingsPresenter(incrementerPreferenceManager);
    }

    @Test
    public void bindListenerTest() {
        settingsPresenter.bindListener(listener);
        verify(listener).setMaximumValue(9);
        verify(listener).setStep(2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setMaximumValueLessZerroTest() {
        settingsPresenter.setMaximumValue(-1);
    }

    @Test
    public void setMaximumValueResetTest() {
        settingsPresenter.bindListener(listener);
        settingsPresenter.setMaximumValue(7);
        verify(incrementerPreferenceManager).setNumber(0);
        verify(incrementerPreferenceManager).setMaximumValue(7);
    }

    @Test
    public void setMaximumValueTest() {
        settingsPresenter.bindListener(listener);
        settingsPresenter.setMaximumValue(15);
        verify(incrementerPreferenceManager, never()).setNumber(0);
        verify(incrementerPreferenceManager).setMaximumValue(15);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setStepLessZerroTest() {
        settingsPresenter.setStep(-1);
    }

    @Test
    public void setStepTest() {
        settingsPresenter.setStep(20);
        verify(incrementerPreferenceManager).setStep(20);
    }

    @Test
    public void resetNumberTest() {
        settingsPresenter.resetNumber();
        verify(incrementerPreferenceManager).setNumber(0);
    }
}
