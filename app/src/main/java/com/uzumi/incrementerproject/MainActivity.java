package com.uzumi.incrementerproject;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.uzumi.incrementerproject.Fragments.CounterFragment.CounterFragment;

/**
 * Main Activity of Incrementer Project.
 */
public class MainActivity extends AppCompatActivity {

    public static final String INCREMENTER_FRAGMENT_TAG = "INCREMENTER_FRAGMENT_TAG";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, new CounterFragment(), INCREMENTER_FRAGMENT_TAG)
                    .commit();
        }
    }
}
