package com.uzumi.incrementerproject.Base;

import android.support.annotation.Nullable;

import com.uzumi.incrementerproject.IncrementerPreferenceManager;

/**
 * Class describing the basic methods of work of the presenter.
 *
 * @param <T> - interface transmitted to class.
 */
public class BasePresenter<T> {

    protected final IncrementerPreferenceManager incrementerPreferenceManager;
    @Nullable
    protected T listener;

    /**
     * Constructor for getting {@link IncrementerPreferenceManager}
     *
     * @param incrementerPreferenceManager - retrieving data from {@link
     *                                     android.content.SharedPreferences}
     */
    public BasePresenter(IncrementerPreferenceManager incrementerPreferenceManager) {
        this.incrementerPreferenceManager = incrementerPreferenceManager;
    }

    /**
     * The function binds the listener of {@link T}.
     *
     * @param listener - A new listener that is set as a new value.
     */
    public void bindListener(T listener) {
        if (this.listener != null) {
            throw new RuntimeException("Listener has been already binded");
        }
        this.listener = listener;
    }

    /**
     * The function unbind listener.
     */
    public void unbindListener() {
        if (listener == null) {
            throw new RuntimeException("Listener has not been binded");
        }
        listener = null;
    }
}
