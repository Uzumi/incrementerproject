package com.uzumi.incrementerproject.Fragments.CounterFragment;

import com.uzumi.incrementerproject.Base.BasePresenter;
import com.uzumi.incrementerproject.IncrementerPreferenceManager;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * The class describes the incrementer logic.
 */
public class CounterPresenter extends BasePresenter<CounterPresenter.Listener> {

    private static final int DEFAULT_NUMBER = 0;

    /**
     * Listener interface for {@link CounterPresenter}.
     */
    public interface Listener {

        /**
         * Set a new current value.
         *
         * @param number - new current value.
         */
        void setNumber(Integer number);
    }

    private AtomicInteger number = new AtomicInteger(DEFAULT_NUMBER);
    private AtomicInteger maximumValue = new AtomicInteger(1000);
    private AtomicInteger step = new AtomicInteger(1);

    /**
     * @param incrementerPreferenceManager - for getting data from {@link
     *                                     android.content.SharedPreferences}
     */
    public CounterPresenter(IncrementerPreferenceManager incrementerPreferenceManager) {
        super(incrementerPreferenceManager);
    }

    @Override
    public void bindListener(Listener listener) {
        super.bindListener(listener);
        setAllData();
        listener.setNumber(incrementerPreferenceManager.getNumber());
    }

    /**
     * The function increments the current value
     */
    public void incrementNumber() {
        number.getAndAdd(step.get());
        if (number.get() >= maximumValue.get() || number.get() < 0) {
            number.set(DEFAULT_NUMBER);
        }
        incrementerPreferenceManager.setNumber(number.get());
        if (listener != null) {
            listener.setNumber(number.get());
        }
    }

    private void setAllData() {
        number.set(incrementerPreferenceManager.getNumber());
        maximumValue.set(incrementerPreferenceManager.getMaximumValue());
        step.set(incrementerPreferenceManager.getStep());
    }
}
