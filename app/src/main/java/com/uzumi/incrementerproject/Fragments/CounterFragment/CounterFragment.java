package com.uzumi.incrementerproject.Fragments.CounterFragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.uzumi.incrementerproject.Fragments.SettingsFragment.SettingsFragment;
import com.uzumi.incrementerproject.IncrementerApplication;
import com.uzumi.incrementerproject.R;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * The fragment where you can increment the value obtained from {@link
 * android.content.SharedPreferences}
 */
public class CounterFragment extends Fragment implements CounterPresenter.Listener {

    private static final String SETTINGS_FRAGMENT_TAG = "SETTINGS_FRAGMENT_TAG";

    @Inject
    CounterPresenter counterPresenter;

    @BindView(R.id.counter_textview)
    TextView incrementerValueTextView;

    private View rootView;
    private Unbinder unbinder;

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_counter, container, false);
        setHasOptionsMenu(true);
        unbinder = ButterKnife.bind(this, rootView);
        IncrementerApplication.getApplication(getActivity().getApplicationContext())
                .getComponentsHolder()
                .getCounterFragmentComponent()
                .inject(this);
        incrementerValueTextView.setOnClickListener(listener -> counterPresenter.incrementNumber());
        counterPresenter.bindListener(this);
        return rootView;
    }

    @Override
    public void setNumber(Integer number) {
        incrementerValueTextView.setText(String.valueOf(number));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        counterPresenter.unbindListener();
        if (getActivity().isFinishing()) {
            IncrementerApplication.getApplication(getActivity().getApplicationContext())
                    .getComponentsHolder()
                    .releaseCounterFragmentComponent();
        }
        unbinder.unbind();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            getActivity()
                    .getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, new SettingsFragment(), SETTINGS_FRAGMENT_TAG)
                    .addToBackStack(SETTINGS_FRAGMENT_TAG)
                    .commit();
        }
        return super.onOptionsItemSelected(item);
    }
}
