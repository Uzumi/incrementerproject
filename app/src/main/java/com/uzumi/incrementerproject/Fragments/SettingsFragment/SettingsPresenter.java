package com.uzumi.incrementerproject.Fragments.SettingsFragment;

import com.uzumi.incrementerproject.Base.BasePresenter;
import com.uzumi.incrementerproject.IncrementerPreferenceManager;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Class describes settings logic for incrementer.
 */
public class SettingsPresenter extends BasePresenter<SettingsPresenter.Listener> {

    private static final int DEFAULT_NUMBER = 0;

    /**
     * Listener interface for {@link SettingsPresenter}.
     */
    public interface Listener {

        /**
         * The function set new maximum value.
         *
         * @param maximumValue - new maximum value.
         */
        void setMaximumValue(int maximumValue);

        /**
         * The function new set step value.
         *
         * @param step - new step value.
         */
        void setStep(int step);
    }

    private AtomicInteger number = new AtomicInteger(DEFAULT_NUMBER);
    private AtomicInteger maximumValue = new AtomicInteger(1000);
    private AtomicInteger step = new AtomicInteger(1);

    /**
     * @param incrementerPreferenceManager - for getting data from {@link
     *                                     android.content.SharedPreferences}
     */
    public SettingsPresenter(IncrementerPreferenceManager incrementerPreferenceManager) {
        super(incrementerPreferenceManager);
    }

    @Override
    public void bindListener(Listener listener) {
        super.bindListener(listener);
        setAllData();
        listener.setMaximumValue(incrementerPreferenceManager.getMaximumValue());
        listener.setStep(incrementerPreferenceManager.getStep());
    }

    /**
     * The function set new maximum value.
     *
     * @param maximumValue - new maximum value.
     */
    public void setMaximumValue(int maximumValue) {
        if (maximumValue < 0) {
            throw new IllegalArgumentException("maximumValue must be equal or greater then zero");
        }
        if (maximumValue <= number.get()) {
            number.set(DEFAULT_NUMBER);
            incrementerPreferenceManager.setNumber(DEFAULT_NUMBER);
        }
        this.maximumValue.set(maximumValue);
        incrementerPreferenceManager.setMaximumValue(maximumValue);
    }

    /**
     * The function set new step.
     *
     * @param step - new step.
     */
    public void setStep(int step) {
        if (step < 0) {
            throw new IllegalArgumentException("step must be equal or greater then zero");
        }
        this.step.set(step);
        incrementerPreferenceManager.setStep(step);
    }

    /**
     * The function set 0 current value.
     */
    public void resetNumber() {
        incrementerPreferenceManager.setNumber(DEFAULT_NUMBER);
        this.number.set(DEFAULT_NUMBER);
    }

    private void setAllData() {
        number.set(incrementerPreferenceManager.getNumber());
        maximumValue.set(incrementerPreferenceManager.getMaximumValue());
        step.set(incrementerPreferenceManager.getStep());
    }
}
