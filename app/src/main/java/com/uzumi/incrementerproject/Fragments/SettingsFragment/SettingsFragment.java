package com.uzumi.incrementerproject.Fragments.SettingsFragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.uzumi.incrementerproject.IncrementerApplication;
import com.uzumi.incrementerproject.R;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * The fragment where you can set new maximum value, set new step value and set to zero current
 * value.
 */
public class SettingsFragment extends Fragment implements SettingsPresenter.Listener {

    private View rootView;
    private Unbinder unbinder;

    @Inject
    SettingsPresenter settingsPresenter;

    @BindView(R.id.edittext_maximum_value)
    EditText maximumValueEditText;

    @BindView(R.id.edittext_step_value)
    EditText stepValueEditText;

    @BindView(R.id.button_reset_value)
    Button resetButton;

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_settings, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        IncrementerApplication.getApplication(getActivity().getApplicationContext())
                .getComponentsHolder()
                .getSettingsFragmentComponent()
                .inject(this);
        settingsPresenter.bindListener(this);
        resetButton.setOnClickListener(listener -> settingsPresenter.resetNumber());
        maximumValueEditText.addTextChangedListener(
                new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        if (!s.toString().isEmpty()) {
                            settingsPresenter.setMaximumValue(Integer.valueOf(s.toString()));
                        }
                    }
                });
        stepValueEditText.addTextChangedListener(
                new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        if (!s.toString().isEmpty()) {
                            settingsPresenter.setStep(Integer.valueOf(s.toString()));
                        }
                    }
                });
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        settingsPresenter.unbindListener();
        if (getActivity().isFinishing()) {
            IncrementerApplication.getApplication(getActivity().getApplicationContext())
                    .getComponentsHolder()
                    .releaseSettingsFragmentComponent();
        }
        unbinder.unbind();
    }

    @Override
    public void setMaximumValue(int maximumValue) {
        maximumValueEditText.setText(String.valueOf(maximumValue));
    }

    @Override
    public void setStep(int step) {
        stepValueEditText.setText(String.valueOf(step));
    }
}
