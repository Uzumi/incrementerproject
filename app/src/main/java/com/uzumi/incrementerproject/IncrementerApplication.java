package com.uzumi.incrementerproject;

import android.app.Application;
import android.content.Context;

/**
 * Class for maintaining global application state extends from {@link Application}
 */
public class IncrementerApplication extends Application {

    private ComponentsHolder componentsHolder;

    @Override
    public void onCreate() {
        super.onCreate();
        componentsHolder = new ComponentsHolder(getApplicationContext());
        componentsHolder.init();
    }

    /**
     * Get application function.
     *
     * @param context - An object that provides access to the basic functions of the application
     * @return - return {@link IncrementerApplication} object.
     */
    public static IncrementerApplication getApplication(Context context) {
        return (IncrementerApplication) context.getApplicationContext();
    }

    /**
     * Function of obtaining a {@link ComponentsHolder} object.
     *
     * @return - current {@link ComponentsHolder} object
     */
    public ComponentsHolder getComponentsHolder() {
        return componentsHolder;
    }
}
