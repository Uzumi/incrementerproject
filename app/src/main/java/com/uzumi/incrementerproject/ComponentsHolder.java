package com.uzumi.incrementerproject;

import android.content.Context;

import com.uzumi.incrementerproject.Components.ApplicationComponent;
import com.uzumi.incrementerproject.Components.CounterFragmentComponent;
import com.uzumi.incrementerproject.Components.DaggerApplicationComponent;
import com.uzumi.incrementerproject.Components.SettingsFragmentComponent;
import com.uzumi.incrementerproject.Modules.ContextModule;

/**
 * Class for working with components.
 */
public class ComponentsHolder {

    private final Context context;
    private ApplicationComponent applicationComponent;
    private CounterFragmentComponent counterFragmentComponent;
    private SettingsFragmentComponent settingsFragmentComponent;

    ComponentsHolder(Context context) {
        this.context = context;
    }

    /**
     * The function of initialization application component.
     */
    public void init() {
        applicationComponent =
                DaggerApplicationComponent.builder().contextModule(new ContextModule(context)).build();
    }

    /**
     * @return - application component.
     */
    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }

    /**
     * Function of obtaining a {@link CounterFragmentComponent} component.
     *
     * @return - counter fragment component.
     */
    public CounterFragmentComponent getCounterFragmentComponent() {
        if (counterFragmentComponent == null) {
            counterFragmentComponent = getApplicationComponent().createCounterFragmentComponent();
        }
        return counterFragmentComponent;
    }

    /**
     * The function release current counter fragment component.
     */
    public void releaseCounterFragmentComponent() {
        counterFragmentComponent = null;
    }

    /**
     * Function of obtaining a {@link SettingsFragmentComponent} component.
     *
     * @return - settings fragment component.
     */
    public SettingsFragmentComponent getSettingsFragmentComponent() {
        if (settingsFragmentComponent == null) {
            settingsFragmentComponent = getApplicationComponent().createSettingsFragmentComponent();
        }
        return settingsFragmentComponent;
    }

    /**
     * The function release current incrementer fragment component.
     */
    public void releaseSettingsFragmentComponent() {
        settingsFragmentComponent = null;
    }
}
