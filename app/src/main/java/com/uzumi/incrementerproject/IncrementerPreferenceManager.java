package com.uzumi.incrementerproject;

import android.content.SharedPreferences;

/**
 * Preference manager for the incrementer, which uses the maximum value and step.
 */
public class IncrementerPreferenceManager {

    private SharedPreferences sharedPreferences;

    public IncrementerPreferenceManager(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    /**
     * @return - current value of the incrementer.
     */
    public int getNumber() {
        return sharedPreferences.getInt(IncrementerKeys.NUMBER_KEY.getKey(), 0);
    }

    /**
     * @return - maximum value of the increment that can be.
     */
    public int getMaximumValue() {
        return sharedPreferences.getInt(IncrementerKeys.MAXIMUM_VALUE_KEY.getKey(), 1000);
    }

    /**
     * @return - step for incrementer operates.
     */
    public int getStep() {
        return sharedPreferences.getInt(IncrementerKeys.STEP_KEY.getKey(), 1);
    }

    /**
     * @param number - new current incrementer value
     */
    public void setNumber(int number) {
        sharedPreferences.edit().putInt(IncrementerKeys.NUMBER_KEY.getKey(), number).commit();
    }

    /**
     * @param maximumValue - new incrementer maximum value
     */
    public void setMaximumValue(int maximumValue) {
        sharedPreferences
                .edit()
                .putInt(IncrementerKeys.MAXIMUM_VALUE_KEY.getKey(), maximumValue)
                .commit();
    }

    /**
     * @param step - new incrementer step.
     */
    public void setStep(int step) {
        sharedPreferences.edit().putInt(IncrementerKeys.STEP_KEY.getKey(), step).commit();
    }

    /**
     * Enum for {@link IncrementerPreferenceManager}
     */
    private enum IncrementerKeys {

        NUMBER_KEY("NUMBER_KEY"),
        MAXIMUM_VALUE_KEY("MAXIMUM_VALUE_KEY"),
        STEP_KEY("STEP_KEY");

        private String key;

        IncrementerKeys(String key) {
            this.key = key;
        }

        public String getKey() {
            return key;
        }
    }
}
