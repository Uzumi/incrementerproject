package com.uzumi.incrementerproject.Modules;

import com.uzumi.incrementerproject.Fragments.CounterFragment.CounterPresenter;
import com.uzumi.incrementerproject.IncrementerPreferenceManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Module providing {@link com.uzumi.incrementerproject.Fragments.CounterFragment.CounterPresenter}
 */
@Module
public class CounterFragmentModule {

    @Provides
    @Singleton
    CounterPresenter provideIncrementerPresenter(
            IncrementerPreferenceManager incrementerPreferenceManager) {
        return new CounterPresenter(incrementerPreferenceManager);
    }
}
