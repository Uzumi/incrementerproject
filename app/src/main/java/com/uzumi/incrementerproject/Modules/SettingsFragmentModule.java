package com.uzumi.incrementerproject.Modules;

import com.uzumi.incrementerproject.Fragments.SettingsFragment.SettingsPresenter;
import com.uzumi.incrementerproject.IncrementerPreferenceManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Module providing {@link com.uzumi.incrementerproject.Fragments.SettingsFragment.SettingsPresenter}
 */
@Module
public class SettingsFragmentModule {

    @Provides
    @Singleton
    SettingsPresenter provideSettingsPresenter(
            IncrementerPreferenceManager incrementerPreferenceManager) {
        return new SettingsPresenter(incrementerPreferenceManager);
    }
}
