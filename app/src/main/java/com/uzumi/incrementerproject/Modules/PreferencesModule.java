package com.uzumi.incrementerproject.Modules;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.uzumi.incrementerproject.IncrementerPreferenceManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Module providing {@link SharedPreferences} and {@link SharedPreferences} for {@link
 * com.uzumi.incrementerproject.Components.ApplicationComponent}
 */
@Module
public class PreferencesModule {

    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    @Provides
    @Singleton
    IncrementerPreferenceManager providePreferencesManager(SharedPreferences sharedPreferences) {
        return new IncrementerPreferenceManager(sharedPreferences);
    }
}
