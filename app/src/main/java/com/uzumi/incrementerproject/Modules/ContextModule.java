package com.uzumi.incrementerproject.Modules;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Module providing context for {@link com.uzumi.incrementerproject.Components.ApplicationComponent}
 */
@Module
public class ContextModule {

    private Context context;

    /**
     * @param context - transmitted into Module context.
     */
    public ContextModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return context;
    }
}
