package com.uzumi.incrementerproject.Components;

import com.uzumi.incrementerproject.Modules.ContextModule;
import com.uzumi.incrementerproject.Modules.PreferencesModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * A component that searches code of creating objects in {@link ContextModule} and {@link
 * PreferencesModule}.
 */
@Component(modules = {ContextModule.class, PreferencesModule.class})
@Singleton
public interface ApplicationComponent {

    /**
     * The function of creating a {@link CounterFragmentComponent}.
     *
     * @return - object of {@link CounterFragmentComponent} class.
     */
    CounterFragmentComponent createCounterFragmentComponent();

    /**
     * The function of creating a {@link SettingsFragmentComponent}.
     *
     * @return - object of {@link SettingsFragmentComponent} class.
     */
    SettingsFragmentComponent createSettingsFragmentComponent();
}
