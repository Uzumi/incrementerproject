package com.uzumi.incrementerproject.Components;

import com.uzumi.incrementerproject.Fragments.CounterFragment.CounterFragment;
import com.uzumi.incrementerproject.Fragments.SettingsFragment.SettingsFragment;
import com.uzumi.incrementerproject.Modules.SettingsFragmentModule;

import javax.inject.Singleton;

import dagger.Subcomponent;

/**
 * A component that searches code of creating objects in {@link SettingsFragmentModule}.
 */
@Subcomponent(modules = {SettingsFragmentModule.class})
@Singleton
public interface SettingsFragmentComponent {

    /**
     * The function of parameters injection for {@link CounterFragment}.
     *
     * @param settingsFragment - component where parameters should be created.
     */
    void inject(SettingsFragment settingsFragment);
}
