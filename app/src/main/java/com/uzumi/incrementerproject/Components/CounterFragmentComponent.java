package com.uzumi.incrementerproject.Components;

import com.uzumi.incrementerproject.Fragments.CounterFragment.CounterFragment;
import com.uzumi.incrementerproject.Modules.CounterFragmentModule;

import javax.inject.Singleton;

import dagger.Subcomponent;

/**
 * A component that searches code of creating objects in {@link CounterFragmentModule}.
 */
@Subcomponent(modules = {CounterFragmentModule.class})
@Singleton
public interface CounterFragmentComponent {

    /**
     * The function of parameters injection for {@link CounterFragment}.
     *
     * @param counterFragment - component where parameters should be created.
     */
    void inject(CounterFragment counterFragment);
}
