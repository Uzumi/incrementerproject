package com.uzumi.incrementerproject.Fragments.SettingsFragment;

import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.preference.PreferenceManager;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v4.app.Fragment;
import android.widget.Button;
import android.widget.EditText;

import com.uzumi.incrementerproject.MainActivity;
import com.uzumi.incrementerproject.R;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by alexandr on 11.06.18.
 */
@RunWith(AndroidJUnit4.class)
public class SettingsFragmentTest {

    @Rule
    public ActivityTestRule<MainActivity> activityTestRule =
            new ActivityTestRule<MainActivity>(MainActivity.class, true, true);

    private SettingsFragment settingsFragment;
    private EditText maximumValueEditText, stepEditText;
    private Button resetButton;

    @Before
    public void setUp() {
        onView(withId(R.id.action_settings)).perform(click());
        initFragment();
    }

    @After
    public void tearDown() {
        SharedPreferences sharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(
                        activityTestRule.getActivity().getApplicationContext());
        sharedPreferences.edit().clear().commit();
    }

    @Test
    public void notNumberTypingMaximumValueTest() {
        onView(withId(R.id.edittext_maximum_value)).perform(typeText("Hello"));
        assertEquals("1000", maximumValueEditText.getText().toString());
    }

    @Test
    public void numberTypingMaximumValueTest() {
        onView(withId(R.id.edittext_maximum_value)).perform(typeText("12"));
        assertNotEquals("100012", maximumValueEditText.getText().toString());
    }

    @Test
    public void numberTypingSignedMaximumValueTest() {
        onView(withId(R.id.edittext_maximum_value)).perform(clearText(), typeText("-312"));
        assertEquals("312", maximumValueEditText.getText().toString());
    }

    @Test
    public void notNumberTypingStepTest() {
        onView(withId(R.id.edittext_step_value)).perform(typeText("Hello"));
        assertEquals("1", stepEditText.getText().toString());
    }

    @Test
    public void numberTypingStepTest() {
        onView(withId(R.id.edittext_step_value)).perform(typeText("12"));
        assertNotEquals("100012", stepEditText.getText().toString());
    }

    @Test
    public void numberTypingSignedStepTest() {
        onView(withId(R.id.edittext_step_value)).perform(clearText(), typeText("-312"));
        assertEquals("312", stepEditText.getText().toString());
    }

    @Test
    public void checkDataAfterRotateTest() {
        String maxValue = maximumValueEditText.getText().toString();
        String step = stepEditText.getText().toString();
        activityTestRule
                .getActivity()
                .setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        initFragment();
        assertEquals(maxValue, maximumValueEditText.getText().toString());
        assertEquals(step, stepEditText.getText().toString());
    }

    private void initFragment() {
        Fragment fragment =
                activityTestRule.getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
        assertNotNull(fragment);
        settingsFragment = (SettingsFragment) fragment;
        maximumValueEditText = settingsFragment.getView().findViewById(R.id.edittext_maximum_value);
        stepEditText = settingsFragment.getView().findViewById(R.id.edittext_step_value);
        resetButton = settingsFragment.getView().findViewById(R.id.button_reset_value);
    }
}
