package com.uzumi.incrementerproject.Fragments.CounterFragment;

import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.preference.PreferenceManager;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v4.app.Fragment;
import android.widget.TextView;

import com.uzumi.incrementerproject.MainActivity;
import com.uzumi.incrementerproject.R;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

@RunWith(AndroidJUnit4.class)
public class CounterFragmentTest {

    @Rule
    public ActivityTestRule<MainActivity> activityTestRule =
            new ActivityTestRule<MainActivity>(MainActivity.class, true, true);

    private CounterFragment counterFragment;
    private TextView counterTextView;

    @Before
    public void setUp() {
        initFragment();
    }

    @After
    public void tearDown() {
        SharedPreferences sharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(
                        activityTestRule.getActivity().getApplicationContext());
        sharedPreferences.edit().clear().commit();
    }

    @Test
    public void checkDataTest() {
        assertEquals("0", counterTextView.getText().toString());
        onView(withId(R.id.counter_textview)).perform(click());
        assertEquals("1", counterTextView.getText().toString());
    }

    @Test
    public void checkDataAfterRotateTest() {
        activityTestRule
                .getActivity()
                .setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        onView(withId(R.id.counter_textview)).perform(click());
        initFragment();
        assertEquals("1", counterTextView.getText().toString());
    }

    private void initFragment() {
        Fragment fragment =
                activityTestRule.getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
        assertNotNull(fragment);
        counterFragment = (CounterFragment) fragment;
        counterTextView = counterFragment.getView().findViewById(R.id.counter_textview);
    }
}
